package com.wyn.myportal.util;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.wyn.myportal.brandstandards.service.BrandStandardsOSGiConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import javax.jcr.Node;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyPortalUtil {

    protected static final Logger log = LoggerFactory.getLogger(com.wyn.myportal.util.MyPortalUtil.class);

    /**
     * Formats the given input date into Associate Press Format.
     * <p/>
     * <p>
     * Returns an empty<code>String</code> if null .
     * </p>
     *
     * @param date
     * @return
     */
    public static String findAPFormattedDate(final Date date) {
        Boolean flag = false;
        final Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        final String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        String formatedString = null;

        try {

            final int year = cal.get(Calendar.YEAR);
            SimpleDateFormat monthParse = new SimpleDateFormat("MMM");
            String month = monthParse.format(date);
            final int currentDate = cal.get(Calendar.DATE);

            for (int i = 0; i < months.length; i++) {
                if (month.equalsIgnoreCase(months[i])) {
                    flag = true;
                    break;
                }
            }

            if (flag) {
                // formatedString = month + ". " + currentDate + ", " + year;
                formatedString = month + " " + currentDate + ", " + year;
            } else if (month.equalsIgnoreCase("sep")) {
                month = "Sept";
                // formatedString = month + ". " + currentDate + ", " + year;
                formatedString = month + " " + currentDate + ", " + year;

            } else {
                monthParse = new SimpleDateFormat("MMMM");
                month = monthParse.format(date);
                formatedString = month + " " + currentDate + ", " + year;
            }

        } catch (Exception e) {
            // LOG.error("Exception in getDateAPFormated()!");
        }

        return formatedString;
    }


    public static String getTagTitle(String namespace, ResourceResolver resourceResolver) {
        String title = "";
        log.debug("Inside getTagTitle(namespace=" + namespace + ")");
        if (StringUtils.isNotBlank(namespace)) {
            String tagPath = MyPortalConstant.TAG_BASEPATH+ StringUtils.replace(namespace, ":" , "/");
            Resource resource =  resourceResolver.getResource(tagPath);
            if(null!=resource)
                title = readNameFromTag(resource);
            else {
                log.debug("No tag title found for: " + namespace);
                title = namespace;
            }
        }
        log.debug("Exiting getTagTitle(namespace=" + namespace + ") = " + title);
        return title;
    }

    public static String getTagTitle(String path, ResourceResolver resourceResolver, String namespace) {
        String title = "";
        log.debug("Inside getTagTitle(path=" + path + ", namespace=" + namespace + ")");
        if (StringUtils.isNotBlank(path)) {
            Resource resource =  resourceResolver.getResource(path);
            if(null!=resource)
                title = readNameFromTag(resource);
            else {
                log.debug("No tag title found for path: " + path);
                title = path;
            }
        }
        log.debug("Exiting getTagTitle(path=" + path + ") = " + title);
        return title;
    }

    public static String getTagID(String path, ResourceResolver resourceResolver, String namespace) {
        String title = "";
        if (StringUtils.isNotBlank(path)) {
            Resource resource =  resourceResolver.getResource(path);
            title = readIdFromTag(resource);
        }
        return title;
    }



    public static String readDescFromTag(Resource tagPathResource) {
        String tagValue = null;
        try {
            Tag tag = tagPathResource.adaptTo(Tag.class);
            Node tagNode = tag.adaptTo(Node.class);
            if (tagNode != null) {
                tagValue = tag.getDescription();
            }
        } catch (Exception ex) {

        }
        return tagValue;

    }

    public static String readNameFromTag(Resource tagPathResource) {
        String tagValue = null;
        try {
            Tag tag = tagPathResource.adaptTo(Tag.class);
            /*Node tagNode = tag.adaptTo(Node.class);
            if (tagNode != null) {
                tagValue = tag.getTitle();
            }*/
            if (tag != null) {
                tagValue = tag.getTitle();
            }
        } catch (Exception ex) {

        }
        return tagValue;

    }


    public static String readIdFromTag(Resource tagPathResource) {
        String tagValue = null;
        try {
            Tag tag = tagPathResource.adaptTo(Tag.class);
            /*Node tagNode = tag.adaptTo(Node.class);
            if (tagNode != null) {
                tagValue = tag.getTagID();
            }*/
            if (tag != null) {
                tagValue = tag.getName();
            }


        } catch (Exception ex) {

        }
        return tagValue;

    }



    public static String getTagID(String namespace, ResourceResolver resourceResolver) {
        String title = "";
        if (StringUtils.isNotBlank(namespace)) {
            String tagPath = MyPortalConstant.TAG_BASEPATH+ StringUtils.replace(namespace, ":" , "/");
            Resource resource =  resourceResolver.getResource(tagPath);
            title = readIdFromTag(resource);
        }
        return title;
    }

    public static String getPageContentType(String templateName) {
        String type = "";
        if (StringUtils.isNotBlank(templateName)) {
            if(MyPortalConstant.CATEGORY_TEMPLATE.contains(templateName))
                type = "category";
            else if(MyPortalConstant.BRANDMATTERS_TEMPLATE.contains(templateName))
                type = "Brand Matters";
            else if(MyPortalConstant.VIDEOPAGE_TEMPLATE.contains(templateName))
                type = "video";
        }
        return type;
    }

    public static String getBrandMattersPageUrl(BrandStandardsOSGiConfiguration osgiConfigService) {
        return  osgiConfigService.getProperty("WCP_HOST_URL")+MyPortalConstant.WCP_BRANDMATTERS;
    }

    public static String getBrandMattersArticle(BrandStandardsOSGiConfiguration osgiConfigService) {
        return  osgiConfigService.getProperty("WCP_HOST_URL")+MyPortalConstant.WCP_BRANDMATTERS_ARTICLE;
    }


    public static List<Resource> iterToList(Iterator<Resource> iter){
        List<Resource> iterList = new ArrayList<Resource>();
        while(iter.hasNext()){
            iterList.add(iter.next());
        }
        Collections.sort(iterList, new Comparator<Resource>() {
            @Override
            public int compare(Resource r1, Resource r2) {
                return (r1.getName().compareTo(r2.getName()));
            }
        });
        return iterList;
    }

    public static String customDateFormatter(Date dt, String dtFormat){
        DateFormat df = new SimpleDateFormat(dtFormat);
        return df.format(dt).toString();
    }

    public static Map<String, String> sortByComparator(Map<String, String> unsortMap) {

        // Convert Map to List
        List<Map.Entry<String, String>> list =
                new LinkedList<Map.Entry<String, String>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, String> sortedMap = new LinkedHashMap<String, String>();
        for (Iterator<Map.Entry<String, String>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, String> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static Map<String, String> getSortedBrandsByName(ResourceResolver resourceResolver) {

        Map<String, String> map = new TreeMap<String, String>();
        try{
            Resource tagResource = resourceResolver.getResource(MyPortalConstant.TAG_BASEPATH+MyPortalConstant.TAG_NAMESPACE+"/"+MyPortalConstant.BRAND_TAG);
            Tag tag = tagResource.adaptTo(Tag.class);
            Iterator<Tag> iter = tag.listChildren();
            while (iter.hasNext()) {
                Tag childTag = iter.next();
                map.put(childTag.getName(),childTag.getTitle());
            }
            map = sortByComparator(map);
        }catch (Exception e){

        }
        return map;
    }


    public static Map<String, String> getBrandsLogo() {

        Map<String, String> map = new HashMap<String, String>();

        map.put("BU" ,"/etc/designs/myportal/clientlibs/images/property-logo-baymont.jpg");
        map.put("DI" ,"/etc/designs/myportal/clientlibs/images/property-logo-daysinn.jpg");
        map.put("PX" ,"/etc/designs/myportal/clientlibs/images/property-logo-night.jpg");
        map.put("BH" ,"/etc/designs/myportal/clientlibs/images/property-logo-hawthorn.jpg");
        map.put("HJ" ,"/etc/designs/myportal/clientlibs/images/property-logo-howard.jpg");
        map.put("KG" ,"/etc/designs/myportal/clientlibs/images/property-logo-knights.jpg");
        map.put("MT" ,"/etc/designs/myportal/clientlibs/images/property-logo-microtel.jpg");
        map.put("RA" ,"/etc/designs/myportal/clientlibs/images/property-logo-ramada.jpg");
        map.put("RAI" ,"/etc/designs/myportal/clientlibs/images/property-logo-ramada.jpg");
        map.put("SE" ,"/etc/designs/myportal/clientlibs/images/property-logo-super.jpg");
        map.put("WT" ,"/etc/designs/myportal/clientlibs/images/property-logo-tryp.jpg");
        map.put("TL" ,"/etc/designs/myportal/clientlibs/images/property-logo-travelodge.jpg");
        map.put("WG" ,"/etc/designs/myportal/clientlibs/images/property-logo-wingate.jpg");
        map.put("WD" ,"/etc/designs/myportal/clientlibs/images/property-logo-wyndham.jpg");

        return map;
    }





}
