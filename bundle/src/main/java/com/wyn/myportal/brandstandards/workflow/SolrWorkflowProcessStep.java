package com.wyn.myportal.brandstandards.workflow;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang3.StringUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;

import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.SolrParams;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.Value;
import java.text.SimpleDateFormat;
// import java.util.*;

import com.wyn.myportal.brandstandards.service.BrandStandardsSolrService;
import com.wyn.myportal.util.MyPortalUtil;
import com.wyn.myportal.brandstandards.service.BrandStandardsOSGiConfiguration;


/**
 * Workflow Step for posting AEM content to Solr. Note that the repository is injected, not
 * retrieved.
 */
@Service
@Properties({
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "Brand Standards Publish to Solr Workflow process implementation"),
		@Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
		@Property(name = "process.label", value = "Publish(v2) Brand Standards to Solr") })
@Component(metatype = false)
public class SolrWorkflowProcessStep implements WorkflowProcess {

	private Session session;
	@Reference
	private SlingRepository repository;
	@Reference
	private QueryBuilder queryBuilder;
	@Reference
	private ResourceResolverFactory resolverFactory;
	@Reference
	private BrandStandardsOSGiConfiguration osgiConfigService;

    @Reference
    private BrandStandardsSolrService solrService;

	/** Default log. */
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	public String getRepositoryName() {
		return repository.getDescriptor(Repository.REP_NAME_DESC);
	}

	public void execute(WorkItem workItem, WorkflowSession wfSession,
                            MetaDataMap metadata) throws WorkflowException {

		try {
			log.debug("Entering execute()");
			String aem_host = osgiConfigService.getProperty("AEM_HOST");
            String zkUrl = osgiConfigService.getProperty("ZK_URL");
            String zkDefaultCollection = osgiConfigService.getProperty("ZK_DEFAULT_COLLECTION");

			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Session session = resourceResolver.adaptTo(Session.class);
			Node node = session.getNode(workItem.getWorkflowData().getPayload().toString()+ "/jcr:content");
            SolrInputDocument solrInputFields =  solrService.getSolrRecord(node,"");
            if( null != solrInputFields){
                //Update Live SOLR
               solrService.updateSolrIndex(solrInputFields, zkUrl, zkDefaultCollection);
            }
			log.debug("Exit: execute()");
		} catch (Exception ex) {
            log.error("Error in : SolrWorkflowProcessStep execute()");
			throw new WorkflowException(ex);
		}
	}
}
