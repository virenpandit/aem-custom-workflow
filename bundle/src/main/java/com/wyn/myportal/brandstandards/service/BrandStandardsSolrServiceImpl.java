package com.wyn.myportal.brandstandards.service;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.wyn.myportal.brandstandards.service.BrandStandardsOSGiConfiguration;
import com.wyn.myportal.util.MyPortalConstant;
import com.wyn.myportal.util.MyPortalUtil;
import org.apache.commons.lang3.StringUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.osgi.framework.Constants;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service(value = BrandStandardsSolrService.class)
@Component(metatype = false)
public class BrandStandardsSolrServiceImpl implements BrandStandardsSolrService {

    private Session session;
    @Reference
    private SlingRepository repository;
    @Reference
    private QueryBuilder queryBuilder;
    @Reference
    private ResourceResolverFactory resolverFactory;
    @Reference
    private BrandStandardsOSGiConfiguration osgiConfigService;

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public SolrInputDocument getSolrRecord(Node node, String aem_host) {

        log.debug("Entering: getSolrRecord().v2");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = null;
        Date date = null;
        String stringDate = null;
        
        try {
            javax.jcr.PropertyIterator iterator = node.getProperties();
            ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);

            Resource pageResource = resourceResolver.getResource(node.getPath());
            ValueMap valueMap = pageResource.adaptTo(ValueMap.class);
            SolrInputDocument sdoc = new SolrInputDocument();

            if (iterator.hasNext()) {
                log.debug("Iterating through node attributes");
                while (iterator.hasNext()) {
                    javax.jcr.Property prop = (javax.jcr.Property) iterator.nextProperty();
                    String propertyName = StringUtils.defaultIfBlank(prop.getName(), "");

                    if(!StringUtils.isBlank(propertyName)) {
                        if ("jcr:uuid".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "id", resourceResolver);
                        } else if ("jcr:title".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "headline", resourceResolver);
                        } else if ("onTime".equalsIgnoreCase(propertyName)) {
                            date = prop.getValue().getDate().getTime();
                            stringDate = MyPortalUtil.findAPFormattedDate(date);
                            String solrPubDt =  MyPortalUtil.customDateFormatter(date, MyPortalConstant.SOLR_DATEFORMATZ);
                            if (solrPubDt != null) {
                                sdoc.addField("publish_dt", solrPubDt);
                            }
                        } else if ("offTime".equalsIgnoreCase(propertyName)) {
                            date = prop.getValue().getDate().getTime();
                            stringDate = MyPortalUtil.findAPFormattedDate(date);
                            String solrExpDt =  MyPortalUtil.customDateFormatter(date, MyPortalConstant.SOLR_DATEFORMATZ);
                            if (solrExpDt != null) {
                                sdoc.addField("expiry_dt", solrExpDt);
                            }
                        } else if ("categories".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "category", resourceResolver);
                        } else if ("coverage".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "coverage", resourceResolver);
                        } else if ("sections".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "section", resourceResolver);
                        } else if ("region".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "region", resourceResolver);
                        } else if ("brand".equalsIgnoreCase(propertyName)) {
                            addFieldToSolrDocument(prop, sdoc, "brand", resourceResolver);
                        }
                    }
                }
            }

            if (!sdoc.isEmpty()) {
                //Adde pagename for the search
                log.debug("Adding pagename: " + node.getParent().getName());
                sdoc.addField("pagename", node.getParent().getName());

                StringBuilder fulltext = executeQuery(node.getPath());
                if (!StringUtils.isEmpty(fulltext.toString())) {
                    log.debug("Adding content_text: " + fulltext.toString());
                    sdoc.addField("content_text", fulltext.toString());
                }
                log.info("Node Path =" + node.getPath());
                // jsonRecord.put("url", AEM_HOST + node.getPath());

                log.debug("Adding uri: " + aem_host + node.getParent().getPath() + ".html");
                sdoc.addField("uri", aem_host + node.getParent().getPath() + ".html");
            } else {
                log.info("Returning empty SolrDocument");
            }

            log.debug("Exit: getSolrRecord().v2, sdoc=" + sdoc.toString());
            log.info(sdoc.toString());
            return sdoc;
        } catch (Exception e){
            e.printStackTrace();
            log.error("Error in Solr" + e.getMessage());
        }
        log.debug("Exiting: getSolrRecord() with NULL");
        return null;
    }


    public void updateSolrIndex(SolrInputDocument solrInputFields, String zkUrl, String zkDefaultCollection) {
        log.debug("Entering: updateSolrIndex.v2(zkUrl=" + zkUrl + ", zkDefaultCollection=" + zkDefaultCollection + ")");

        try{
            CloudSolrServer zkServer = new CloudSolrServer(zkUrl);
            log.debug("Connecting to Zookeeper: " + zkUrl);
            zkServer.connect();
            zkServer.setDefaultCollection(zkDefaultCollection); // the base collection
            UpdateResponse resp = zkServer.add(solrInputFields);
            UpdateResponse res = zkServer.commit();
            log.debug(".v2: Brand Standards data successfully transmitted to Solr Server");
            zkServer.shutdown();
        }
        catch (Exception e){
            e.printStackTrace();
            log.error("Error in updating SOLR index" + e.getMessage() );
        }
    }

    public StringBuilder executeQuery(String path) throws Exception {
        log.debug("Entering : executeQuery()");
        StringBuilder builder = new StringBuilder();

        try {
            ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            session = resourceResolver.adaptTo(Session.class);
            Map<String, String> map = new HashMap<String, String>();
            log.info("PATH: " + path);
            map.put("path", path);
            map.put("property", "text");
            map.put("property.operation", "exists");
            Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
            query.setStart(0);
            SearchResult result = query.getResult();
            builder = new StringBuilder();
            Iterator<Node> nodeItr = result.getNodes();
            while (nodeItr.hasNext()) {
                Node node = nodeItr.next();
                if (node.hasProperty("text")) {
                    builder.append(node.getProperty("text").getValue().getString());
                }
            }
        } catch (Exception ex) {
            log.error("Exception occurred in deleteFromSolrSearch:" + ex);
            ex.printStackTrace();
        } finally {
            if (null != session) {
                session.logout();
            }
        }

        log.debug("Exit: executeQuery()");
        return builder;
    }

    private void addFieldToSolrDocument(javax.jcr.Property prop, SolrInputDocument sdoc, String fieldName, 
                                            ResourceResolver resourceResolver) throws Exception {

        log.debug("Inside addFieldToSolrDocument() property=" + prop);
        ArrayList<String> list = new ArrayList<String>();
        if(prop.isMultiple()) {
            Value[] value = prop.getValues();
            log.debug("Adding attribute[]: " + fieldName + "=" + value);

            if (!StringUtils.isEmpty(fieldName) && value.length > 0) {
                for (int i=0; i<value.length; i++) {
                    list.add(MyPortalUtil.getTagTitle(value[i].getString(), resourceResolver, ""));
                }
                sdoc.addField(fieldName, list);
            }
        } else{
            String value = (String) prop.getValue().getString();
            value = MyPortalUtil.getTagTitle(value, resourceResolver, "");
            log.debug("Adding attribute: " + fieldName + "=" + value);
            if (!StringUtils.isEmpty(fieldName) && !StringUtils.isEmpty(value)) {
                sdoc.addField(fieldName, value);
            }
        }
        log.debug("Exiting addFieldToSolrDocument()");
    }
}
