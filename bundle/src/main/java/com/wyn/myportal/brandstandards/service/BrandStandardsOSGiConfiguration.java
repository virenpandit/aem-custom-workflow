package com.wyn.myportal.brandstandards.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
//import java.util.Enumeration;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.SlingServletException;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Implementation of BrandStandardsOSGiConfiguration provides OsgiConfig parameters.
*/
@Service(value={
        BrandStandardsOSGiConfiguration.class,
        javax.servlet.Servlet.class
 })

@Component(immediate = true, metatype = true)
@Properties({
        @Property(name="AEM_HOST", label="AEM HOST", value="http://localhost:4502"),
        @Property(name="SOLR_HOST", label="SOLR HOST", value="http://vsvphxslrdev01.hotelgroup.com:9992/solr"),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Brand Standards Service Configuration"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
        @Property(name = "process.label", value = "Brand Standards - Configuration Service"),
        @Property(name = "ZK_URL", label="ZOOKEEPER URL", value = "http://vsvphxslrdev01.hotelgroup.com:9095"),
        @Property(name = "ZK_DEFAULT_COLLECTION", label="ZOOKEEPER DEFAULT COLLECTION", value = "brandstandards"),
        @Property(name = "BrandStd-Solr-Properties", value = { "jcr:uuid-id", "jcr:title-headline", "cq:lastPublished-publish_dt", "expirydate-expiry_dt",
                "language-language", "cq:brandTags-brand_tags", "cq:categoryTags-category_tags",
                "cq:languageTags-language_tags", "cq:regionTags-region_tags", "cq:roleTags-role_tags", "cq:sitestatusTags-sitestatus_tags" }),
		@Property(name="sling.servlet.paths", value = "/bin/config"),
        @Property(name="sling.servlet.selectors",value="brandstandards"),
        @Property(name="sling.servlet.extensions",value="json")
})

public class BrandStandardsOSGiConfiguration extends SlingAllMethodsServlet {

	private Dictionary<String, String> properties;
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	    protected void activate(ComponentContext context) {
	        properties = context.getProperties();
	        //Check the properties are reading from OsgiConfig
	        /*for (Enumeration<String> e = properties.keys(); e.hasMoreElements();) {
	        	       log.info("Key value from OSGI Config : " + (String) e.nextElement());
	        }*/
	    }

	    protected void deactivate(ComponentContext context) {
	        properties = null;
	    }

	    public String getProperty(String key) {
	    	String config_key = null;
	    	if(null != properties){
	    		config_key = properties.get(key);
	    		 log.info("Osgi config value of " + key + " is: "+ config_key);
	    	}
	        return config_key;     
	    } 

	    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws SlingServletException, IOException {
			PrintWriter out = response.getWriter();
			String property = request.getParameter("param");
			String value = "";
			if(StringUtils.isNotBlank(property)){
				value = getProperty(property);
			}
			out.write(value);

		}		
}
