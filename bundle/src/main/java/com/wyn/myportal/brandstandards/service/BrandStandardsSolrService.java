package com.wyn.myportal.brandstandards.service;

import org.apache.solr.common.SolrInputDocument;

import javax.jcr.Node;

public interface BrandStandardsSolrService {

    public SolrInputDocument getSolrRecord(Node node, String aem_host);
    public void updateSolrIndex(SolrInputDocument solrInputFields, String zk_url, String zkDefaultCollection);

}
